import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import React, {Component} from 'react';

const Tombol = ({label, onPress}) => {
  return (
      <TouchableOpacity onPress={onPress} style={styles.container}>
        <Text>{label}</Text>
      </TouchableOpacity>
  );
};

export class Home extends Component {
  render() {
    return (
      <View>
        <Tombol
          label={'Halaman Satu'}
          onPress={() => this.props.navigation.navigate('Halaman Satu')}
        />
        <Tombol
          label={'Halaman Dua'}
          onPress={() => this.props.navigation.navigate('Halaman Dua')}
        />
        <Tombol 
          label={'Halaman Tiga'}
          onPress={() => this.props.navigation.navigate('Halaman Tiga')}/>
      </View>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    margiTop:15,
    margin: 15,
    marginLeft: 0,
    padding: 15,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: '#20b2aa', //lightseagreen
  },
});
