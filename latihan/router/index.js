import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Home, HalamanSatu, HalamanDua, HalamanTiga} from '../pages';
const Navigasi = createNativeStackNavigator();

const Route = () => {
  return (
    <Navigasi.Navigator>
      <Navigasi.Screen name="Home" component={Home} />
      <Navigasi.Screen name="Halaman Satu" component={HalamanSatu} />
      <Navigasi.Screen name="Halaman Dua" component={HalamanDua} />
      <Navigasi.Screen name="Halaman Tiga" component={HalamanTiga} />
    </Navigasi.Navigator>
  );
};

export default Route;