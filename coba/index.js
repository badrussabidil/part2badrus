import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import React, {Component} from 'react';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warnaText: '#000', //hitam
      inputPertama: '',
      inputKedua: '',
      fullText: '',
    };
  };

  gabungData = () => {
    let inputPertama = this.state.inputPertama; 
    let inputKedua = this.state.inputKedua;
    this.setState({fullText: inputPertama + " " + inputKedua})
  };

  // ubahWarna = () => {
  //   const warna = this.state.warnaText;
  //   if (warna === 'black') {
  //     this.setState({warnaText: 'red'});
  //   } else if (warna === 'red') {
  //     this.setState({warnaText: 'blue'});
  //   } else if (warna === 'blue') {
  //     this.setState({warnaText: 'green'});
  //   } else {
  //     this.setState({warnaText: 'black'});
  //   };
  // };


  render = () => {
    return (
      <View style={styles.container}>
        <Text style={{ color: this.state.warnaText, fontSize: 30, margin:10, }}>ISI FORM DI BAWAH INI</Text>
        <TextInput style={styles.input} placeholder="Nama Pertama" onChangeText={inputPertama => this.setState({inputPertama})}/>
        <TextInput style={styles.input} placeholder="Nama Kedua"onChangeText={inputKedua => this.setState({inputKedua})}/>
        <Text style={styles.fulltext}>Full Text : {this.state.fullText}</Text>
        <TouchableOpacity onPress={() => this.gabungData()} style={styles.btn}>
          <Text style={{ color: '#ff0000' }}>tekan</Text>
        </TouchableOpacity>
      </View>
    );
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: '#87ceeb' //skyblue
  },
  btn: {
    backgroundColor: '#fff', //putih
    padding: 10,
    margin: 10,
  },
  input: {
    backgroundColor: '#20b2aa', //lightseagreen
    margin: 15,
    width: '70%',
    paddingLeft: 25,
  },
  fulltext: {
    marginTop: -5,
    color: '#ff1493'//deepping
  }
});

export default Home;